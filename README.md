## Problems
When you have some problem with my packages or repository, for first check opened or pinned [issues](https://github.com/hawkeye116477/waterfox-deb-rpm-arch-AppImage/issues?q=is%3Aopen+is%3Aissue) and [discussions](https://github.com/hawkeye116477/waterfox-deb-rpm-arch-AppImage/discussions?discussions_q=is%3Aopened).

## Adding repository/direct download

* If you're using Debian or Ubuntu with KDE:

[Click to see instructions for waterfox-classic-kde.](https://software.opensuse.org//download.html?project=home%3Ahawkeye116477%3Awaterfox&package=waterfox-classic-kde)

[Click to see instructions for waterfox-g-kde.](https://software.opensuse.org//download.html?project=home%3Ahawkeye116477%3Awaterfox&package=waterfox-g-kde)

* If you're using other environment than KDE (Gnome, XFCE, etc) or other distro than Debian or Ubuntu:

[Click to see instructions for waterfox-classic-kpe.](https://software.opensuse.org//download.html?project=home%3Ahawkeye116477%3Awaterfox&package=waterfox-classic-kpe)

[Click to see instructions for waterfox-g-kpe.](https://software.opensuse.org//download.html?project=home%3Ahawkeye116477%3Awaterfox&package=waterfox-g-kpe)


------
### Note 1: KPE means KDE Plasma Edition, but package is compatible with other DE also and doesn't depend on KDE libs. Hovewer for KDE I recommend installing package waterfox-classic-kde, which also installs other depends required for proper working of KDE integration features.

### Note 2: Grabbing packages directly doesn't always work without errors. If you got error about unmet depends, then  you need to lookup what depends it needs (see control file for package), then download and install them manually or just do the step with adding repository instead of.

### Note 3: Language packs are available as separate packages to install from apt repository! You can also download and install it directly from [download.opensuse.org/repositories/home:/hawkeye116477:/waterfox/xUbuntu_18.04/all](https://download.opensuse.org/repositories/home:/hawkeye116477:/waterfox/xUbuntu_18.04/all).


## Downloading and installing AppImage packages

[Click to see instructions for Waterfox Classic.](https://appimage.github.io/Waterfox_Classic/)

[Click to download latest Waterfox G.](https://download.opensuse.org/repositories/home:/hawkeye116477:/waterfox/AppImage/waterfox-g-latest-x86_64.AppImage.mirrorlist)

## Other source files

[Click to see other source files for waterfox-classic-kpe.](https://build.opensuse.org/package/show/home:hawkeye116477:waterfox/waterfox-classic-kpe)

[Click to see other source files for waterfox-g-kpe.](https://build.opensuse.org/package/show/home:hawkeye116477:waterfox/waterfox-g-kpe)

[Click to see other source files for Waterfox Classic AppImage Edition.](https://build.opensuse.org/package/show/home:hawkeye116477:waterfox/waterfox-classic-appimage)

[Click to see other source files for Waterfox G AppImage Edition.](https://build.opensuse.org/package/show/home:hawkeye116477:waterfox/waterfox-g-appimage)

## Patches
Latest Waterfox Classic KDE Plasma Edition contains following patches: [github.com/hawkeye116477/waterfox-deb/tree/master/waterfox-classic-kpe/patches](https://github.com/hawkeye116477/waterfox-deb/tree/master/waterfox-classic-kpe/patches).

Latest Waterfox G KDE Plasma Edition contains following patches: [github.com/hawkeye116477/waterfox-deb/tree/master/waterfox-g-kpe/patches](https://github.com/hawkeye116477/waterfox-deb/tree/master/waterfox-g-kpe/patches).

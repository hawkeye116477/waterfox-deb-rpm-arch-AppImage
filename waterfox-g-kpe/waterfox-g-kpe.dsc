Format: 3.0 (quilt)
Source: waterfox-g-kpe
Binary: waterfox-g-kpe
Architecture: any
Version: 0.2.1-1
Maintainer: hawkeye116477 <hawkeye116477@gmail.com>
Homepage: https://www.waterfox.net
Standards-Version: 3.9.7
Build-Depends: debhelper (>= 9), autoconf2.13, libgtk-3-dev (>= 3.14), libdbus-glib-1-dev, libpulse-dev, libasound2-dev, yasm (>= 1.1), build-essential, libxt-dev, python3 (>= 3.5), zip, unzip, cargo (>= 0.59), libgl1-mesa-dev, libnotify-dev (>= 0.4), binutils-avr, libfreetype6-dev (>= 2.0.1), libfontconfig1-dev, pkg-config, libtinfo-dev, clang (>= 5.0) | clang-10 | clang-11 | clang-12 | clang-13, llvm-dev (>= 5.0) | llvm-10-dev | llvm-11-dev | llvm-12-dev | llvm-13-dev, lld (>= 5.0) | lld-10 | lld-11 | lld-12 | lld-13, rustc (>= 1.59.0~), libxext-dev, libglib2.0-dev (>= 2.18), libpango1.0-dev (>= 1.14.0), libstartup-notification0-dev, libcurl4-openssl-dev, libiw-dev, mesa-common-dev, libxrender-dev, dbus-x11, xvfb, libx11-dev, libx11-xcb-dev, libfile-fcntllock-perl, apt-utils, locales, autotools-dev, libjpeg-dev, zlib1g-dev, libreadline-dev, dpkg-dev (>= 1.16.1.1~), libevent-dev (>= 1.4.1), libjsoncpp-dev, xfonts-base, xauth, lsb-release, cbindgen (>= 0.24.2), nodejs (>= 10.21) | nodejs-mozilla (>= 10.21), libjack-dev, nasm (>= 2.14) | nasm-mozilla (>= 2.14), libclang-dev (>= 5.0) | libclang-10-dev | libclang-11-dev | libclang-12-dev | libclang-13-dev, libstdc++6 (>= 7.0) | gcc-mozilla (>= 7), bc
Package-List:
 waterfox-g-kpe deb web optional arch=any
Files:
 0000000000000000000000000000000 1 waterfox-g-kpe.orig.tar.xz
 0000000000000000000000000000000 1 waterfox-g-kpe.debian.tar.xz
# https://github.com/openSUSE/obs-build/pull/147
DEBTRANSFORM-RELEASE: 1
